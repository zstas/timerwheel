#define BOOST_TEST_MODULE boost_test_macro3
#include <boost/test/included/unit_test.hpp>

#include <timer_wheel.hpp>
#include <functional>
#include <thread>
#include <atomic>

void cbAdd( int &x ) {
    x++;
}

BOOST_AUTO_TEST_CASE( test_op_reportings )
{
    int c1count = 0;
    int c2count = 0;
    int c3count = 0;
    int c4count = 0;
    int c5count = 0;

    TimerWheel tw;
    auto c1 = std::make_shared<TimerEvent>( 1000, std::bind( cbAdd, std::ref( c1count )) );
    auto c2 = std::make_shared<TimerEvent>( 333, std::bind( cbAdd, std::ref( c2count )) );
    auto c3 = std::make_shared<TimerEvent>( 200, std::bind( cbAdd, std::ref( c3count )) );
    auto c4 = std::make_shared<TimerEvent>( 20, std::bind( cbAdd, std::ref( c4count )) );
    auto c5 = std::make_shared<TimerEvent>( 1, std::bind( cbAdd, std::ref( c5count )) );
    tw.registerCB( c1 );
    tw.registerCB( c2 );
    tw.registerCB( c3 );
    tw.registerCB( c4 );
    tw.registerCB( c5 );
    std::mutex mtx;

    std::atomic<bool> start { false };
    auto threadFun = [ &tw, &start, &mtx, c1, c2, c3, c4, c5 ]( int tNum ) {
        while( !start ) {}
        tw.onNewTime( 100 );

        {
            std::lock_guard<std::mutex> lg { mtx };
            c1->timeout = 3;
            c2->timeout = 10000;
            c3->scheduled = false;
        }
        tw.onNewTime( 500 );

        {
            std::lock_guard<std::mutex> lg { mtx };
            c3->scheduled = true;
        }
        tw.onNewTime( 1000 );
    };

    std::vector<std::thread> threads;
    for( int i = 0; i < 10; i++ ) {
        threads.emplace_back( threadFun, i );
    }

    start = true;

    for( int i = 0; i < 10; i++ ) {
        threads[i].join();
    }

    BOOST_TEST( c1count == 1 );
    BOOST_TEST( c2count == 1 );
    BOOST_TEST( c3count == 0 );
    BOOST_TEST( c4count == 50 );
    BOOST_TEST( c5count == 1000 );
}