#ifndef TIMER_WHEEL_HPP
#define TIMER_WHEEL_HPP

#include <functional>
#include <list>
#include <memory>
#include <mutex>

static constexpr auto numBuckets { 255U };
static constexpr auto numLevels { 3U };
static constexpr auto granularityPerMs { 1000U }; // make it 1 sec

using cbtype_t = std::function<void(void)>;

class TimerEvent {
public:
    TimerEvent() = delete;
    TimerEvent( const TimerEvent & ) = delete;
    TimerEvent( TimerEvent && ) = delete;

    TimerEvent( uint64_t t, cbtype_t c ):
        timeout( t ),
        cb( c )
    {}

    TimerEvent& operator=( TimerEvent&& ) = delete;
    TimerEvent& operator=( const TimerEvent &) = delete;

    bool scheduled;
    uint64_t timeout;
    cbtype_t cb;
    uint64_t remaining;
};

struct TimerBucket {
    uint64_t current;
    std::list<std::shared_ptr<TimerEvent>> buckets[numBuckets];
};

class TimerWheel {
public:
    TimerWheel():
        last_run( 0 )
    {}

    TimerWheel( struct timeval tv );

    TimerWheel( const TimerWheel & ) = delete;
    TimerWheel( TimerWheel && ) = delete;
    TimerWheel& operator=( TimerWheel&& ) = delete;
    TimerWheel& operator=( const TimerWheel &) = delete;

    void registerCB( std::shared_ptr<TimerEvent> event, uint64_t timeout );
    void registerCB( std::shared_ptr<TimerEvent> event );
    void onNewTime( uint32_t ticks );
    void onNewTime( struct timeval tv );
private:
    void tick();
    uint64_t last_run;
    TimerBucket wheels[ numLevels ];
    std::mutex mtx;
};

#endif