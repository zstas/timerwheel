#include <iostream>
#include "timer_wheel.hpp"

TimerWheel::TimerWheel( struct timeval tv ) {
    uint64_t ms = tv.tv_sec * 1000 + tv.tv_usec / 1000;
    last_run = ms / granularityPerMs;
}

void TimerWheel::registerCB( std::shared_ptr<TimerEvent> event ) {
    registerCB( event, event->timeout );
}

void TimerWheel::registerCB( std::shared_ptr<TimerEvent> event, uint64_t timeout ) {
    int level = 0;
    uint64_t r = 0;
    event->remaining = 0;
    uint32_t multiplier = 1;

    while( level < numLevels ) {
        auto &cur_wheel = wheels[ level ];
        r = ( cur_wheel.current + timeout ) % numBuckets;
        timeout = ( cur_wheel.current + timeout ) / numBuckets;

        if( timeout == 0 )
        break;

        event->remaining += r * multiplier;
        multiplier *= numBuckets;
        level++;
        continue;
    }

    if( level == numLevels )
        event->remaining += multiplier * (timeout - 1);
 
    event->scheduled = true;
    wheels[ level ].buckets[ r ].push_back( std::move( event ) );
}

void TimerWheel::onNewTime( uint32_t ticks ) {
    std::lock_guard<std::mutex> lg { mtx };
    while( last_run < ticks )
        tick();
}

void TimerWheel::onNewTime( struct timeval tv ) {
    uint64_t ms = tv.tv_sec * 1000 + tv.tv_usec / 1000;
    onNewTime( ms / granularityPerMs );
}

void TimerWheel::tick() {
    int level = 0;
    uint64_t n = 0;

    do {
        n = ( wheels[ level ].current + 1 ) % numBuckets;
        wheels[ level ].current = n;

        auto &cur_wheel = wheels[level].buckets[ n ];
        for( auto &event: cur_wheel ) {
            if( event->remaining > 0 ) {
                registerCB( event, event->remaining );
                continue;
            }

            if( !event->scheduled )
                continue;

            if( event->cb )
                event->cb();

            registerCB( event );
        }
        cur_wheel.clear();

        level++;
    } while( n == 0 && level < numLevels );

    last_run++;
}