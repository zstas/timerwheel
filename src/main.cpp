#include <iostream>

#include "timer_wheel.hpp"

void callback1() {
    std::cout << "callback1" << std::endl;
}

void callback2() {
    std::cout << "callback2" << std::endl;
}

int main( int argc, char *argv[] ) {
    struct timeval aCurrentTime;

    TimerWheel tw;
    auto c1 = std::make_shared<TimerEvent>( 10, &callback1 );
    auto c2 = std::make_shared<TimerEvent>( 20, &callback2 );

    tw.registerCB( c1 );
    tw.registerCB( c2 );

    for( int i = 0; i < 0xFFFFFF + 1; i++ ) {
        tw.onNewTime( i );
        if( i == 900 )
            c1->timeout = 3;
    }
    tw.onNewTime( 10 );

    return 0;
}