Timer wheel
===========

Descripton
----------

There is an example of timer wheel with 3 levels (configurable via editing header).
Granularity of the wheel is 1 second (also configurable via header).

Dependecies
-----------
* c++14
* pthread (for several multithreaded tests)
* boost for boost.test module

How to build
------------
```
mkdir build
cmake -DCMAKE_BUILD_TYPE=DEBUG ..
cd build
make -j$(cat /proc/cpuinfo | grep processor | wc -l) || make
make test
```


Tests
-----
* 1 thread, offline mode, ticks
* 1 thread, online mode, ticks
* 1 thread, offline mode, struct timeval
* 10 threads, offline mode, ticks
* 10 threads, online mode, ticks, with random sleep
* 10 threads, offline, editing timer on the fly (changing timeout and disabling)
